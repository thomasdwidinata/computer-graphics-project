/*description:
  encapsulation of rendering scene, camera, and light

  parameter:
  - width: width of a ground (OBSOLTE SOON after dynamic tiling is made)
  - length: length of a ground (OBSOLTE SOON after dynamic tiling is made)

*/
function World (name, width, length){
  this.name=name;
  this.width=width;
  this.height=length;

  this.scene = new BABYLON.Scene(engine);
  var skybox = BABYLON.Mesh.CreateBox("skyBox", 150, this.scene);
    var skyboxMaterial = new BABYLON.StandardMaterial("skyBox", this.scene);
    skyboxMaterial.backFaceCulling = false;
    skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("skybox/skybox", this.scene);
    skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
    skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
    skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
    skyboxMaterial.disableLighting = true;
    skybox.material = skyboxMaterial;



  this.light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0,100,-50), this.scene);
  this.light.intensity=.8;
  this.light.specular = new BABYLON.Color3 (1,.8,.6,.8);
  this.light.diffuse = new BABYLON.Color3 (1,1,1,.2);
  this.light.groundColor = new BABYLON.Color3 (1,.8,.6,1);


  this.camera = new BABYLON.FreeCamera("sceneCamera", new BABYLON.Vector3(0, 30, 0), this.scene);
  console.log(this.camera);

    var xmin = -50;
      var zmin = -50;
      var xmax = 50;
      var zmax =  50;
    var precision = {
        "w" : 1,
        "h" : 1
    };
    var subdivisions = {
        'h' : 10,
        'w' : 10
    };
    // Create the Tiled Ground
  this.tiledGround = new BABYLON.Mesh.CreateTiledGround("Tiled Ground", xmin, zmin, xmax, zmax, subdivisions, precision,this.scene);
  this.multiMatGround = new BABYLON.MultiMaterial("multiMatGround", this.scene);

  this.matGround = new BABYLON.StandardMaterial("matGround1",this.scene);
  this.matGround.diffuseTexture = new BABYLON.Texture("assets/material/ground_tile.jpg", this.scene);
  this.matGround.diffuseTexture.wrapU = BABYLON.Texture.CLAMP_ADDRESSMODE;
  this.matGround.diffuseTexture.wrapV = BABYLON.Texture.CLAMP_ADDRESSMODE;
  this.multiMatGround.subMaterials.push(this.matGround);
  this.multiMatGround.specularColor = new BABYLON.Color4(0, 0, 0, 1);
  this.tiledGround.material = this.multiMatGround;
  this.setCameraTarget = function (mesh){
    this.camera.target = mesh; // target any mesh or object with a "position" Vector3
    console.log("target");
    console.log(this.camera.target);
    this.camera.position = new BABYLON.Vector3(mesh.position.x,mesh.position.y+60,mesh.position.z-30);
    this.camera.setTarget(new BABYLON.Vector3(mesh.position.x,mesh.position.y+5,mesh.position.z));
  }
  this.cameraFollow=function(){
    var disX = this.camera.position.x - this.camera.target.position.x;
    var disZ = this.camera.position.z - this.camera.target.position.z;
    var X_THRESHOLD_VALUE = 40;
    var Z_THRESHOLD_VALUE = 20;

    if (disX > X_THRESHOLD_VALUE || disX <=-X_THRESHOLD_VALUE || disZ > -20|| disZ <=-50){
      if (this.camera.target.moveDirection!=undefined){
        this.camera.position.x+=this.camera.target.moveDirection.xDir;
        this.camera.position.z+=this.camera.target.moveDirection.zDir;
      }
    }
  }
  this.readRotatation = function (){
    if (this.camera.isRotate =="CCW"){
      this.camera.rotation.y-=0.02;
    }
    else if (this.camera.isRotate =="CW"){
      this.camera.rotation.y+=0.02;
    }
  }

  /*
    lose(a mesh)
    description:
    - return true if a mesh is outside the ground, false otherwise
  */
  this.lose = function (mesh){
    if(mesh.position.x>width/2 || mesh.position.x<-width/2|| mesh.position.z>length/2 || mesh.position.z<-length/2)
       return true;
   }
}
