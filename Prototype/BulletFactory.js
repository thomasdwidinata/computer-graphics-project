/*these codes are about Bullet, BulletPool, and BulletFactory*/

/*description:
  as the name goes, this is a factory class that holds blueprints of default bulletFactoryScript
  two known default bullets are player's and enemy's

  note: currently this is not yet singleton
*/
function BulletFactory(){

    /*
      defaultPlayerBullet(the rendering scene)
      description:
      return a player bullet

      known call:
      - construction of a BulletPool

      note: this is not static yet
    */
    this.defaultPlayerBullet=function(scene){
      var bullet = new Bullet (25,scene, DEFAULT_PLAYER_BREAKABLE);
      bullet.visibility=0;
      return bullet;
    }

    /*
      defaultEnemyBullet(the rendering scene)
      description:
      return an enemy bullet

      known call:
      - construction of a BulletPool

      note: this is not static yet
    */
    this.defaultEnemyBullet=function(scene){
      var bullet = new Bullet(25,scene, DEFAULT_ENEMY_BREAKABLE);
      var bulletMat=new BABYLON.StandardMaterial("enemyBulletMat", scene);
      bulletMat.diffuseColor= new BABYLON.Color3(1,0,0);
      bullet.mesh.material = bulletMat;
      bullet.visibility=0;
      return bullet;
    }
}

/*description:
  Object pooling. Pool of bullets

  parameter:
  - scene: the rendering scene
  - type: type of bullet you wish to pool : currently only ["DEFAULTPLAYER"|"DEFAULTENEMY"]
  - rate: predicted firing rate of the bullet. Will determine how many object should be
          prepared in the pool

  note: later, it will evolved into a customisable pool. There will be new types (hopefully)
*/
function BulletPool (scene, type, rate){
  this.poolCount = 0;
  this.rate=rate;
  var bulletFactory = new BulletFactory();
  this.pool = [];

  if (type=="DEFAULTPLAYER"){
    for (var i=0; i<this.rate; i++){
      this.pool[i] = bulletFactory.defaultPlayerBullet(scene);
    }
  }
  else if (type=="DEFAULTENEMY"){
    for (var i=0; i<this.rate; i++){
      this.pool[i] = bulletFactory.defaultEnemyBullet(scene);
    }
  }

}

/*
  popBullet(....)
    - initX, initY, initZ: bullet initial position
    - trgX, trgY, trgZ: where should the bullet move
    note: the bullet will still move after reaching trgX, trgY, trgZ, mimicking a bullet in real-life

  description:
  1. activate(visibility on) a bullet from the pool. starts from index 0 to the last index, go to 0 again
  2. initiate Bullet.fire(), so active bullets are automatically moving to target
*/
BulletPool.prototype.popBullet=function(initX, initY, initZ,trgX, trgY, trgZ){
  if (this.poolCount>=this.rate)
    this.poolCount=0;
  else {
    this.poolCount++;
  }
  this.pool[this.poolCount].position = new BABYLON.Vector3(initX, initY, initZ);
  this.pool[this.poolCount].mesh.visibility=1;
  console.log("init x: "+initX);
  console.log("init y: "+initY);
  console.log("init z: "+initZ);

  this.pool[this.poolCount].fire(initX, initY, initZ, trgX, trgY, trgZ);
}


/*description:
  A bullet, invisible and immobilize after the construction
  it is highly recommended that a bullet can only be made by a BulletFactory and
  modified by a BulletPool

  parameter:
  - speed: interval of moving 1 vector point (in milliseconds); the smaller the faster
  - scene: the rendering scene
  - breakable: things that can be broken by this bullet
    (ex: enemy's bullet breaks DEFAULT_ENEMY_BREAKABLE)
*/

function Bullet (speed, scene, breakable){
    BABYLON.Mesh.call(this,"defaultBullet", scene);

    this.speed = speed;
    this.mesh = new BABYLON.Mesh.CreateSphere("enemyBullet", 3, 1, scene);
    this.mesh.visibility=0;
    this.mesh.parent=this;
    this.active = false;
    this.breakable = breakable;
}

Bullet.prototype = Object.create(BABYLON.Mesh.prototype);

/*
  fire(...)
  - initX, initY, initZ: bullet initial position
  - trgX, trgY, trgZ: where should the bullet move

  description:
  1. calculating where should the bullet move
  2. initiate moving interval
    (in moving interval)
    2.1. increment its position
    2.2. initiate its breakable detection (this.breakable.detect(itself))
    2.3. stop the interal under a certain condition
*/
Bullet.prototype.fire = function (initX, initY, initZ, trgX, trgY, trgZ){
  this.initialPos = new BABYLON.Vector3(initX, initY, initZ);
  this.active = true;
  this.target = new BABYLON.Vector3(trgX, trgY, trgZ);

  this.distance = Math.pow(this.initialPos.x-trgX,2)+Math.pow(this.initialPos.z-trgZ,2);
  this.distance = Math.sqrt(this.distance + Math.pow(this.initialPos.y-trgY,2));

  this.addX = (this.target.x-this.initialPos.x)/this.distance;
  this.addY = (this.target.y-this.initialPos.y)/this.distance;
  this.addZ = (this.target.z-this.initialPos.z)/this.distance;

  var that = this;

  this.firing = setInterval(function(){
    that.breakable.detecting = true;
    that.position.x+=that.addX;
    that.position.z+=that.addZ;
    that.position.y+=that.addY;
    that.breakable.detect(that);
    if (activeScene){
      if (activeScene.lose(that)){
        that.breakable.detecting=false;
        that.mesh.visibility=0;
        clearInterval(that.firing);
      }
    }
  },that.speed);

}
