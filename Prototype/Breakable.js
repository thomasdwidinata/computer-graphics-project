/*description: List of breakable objects
  breakable means something will happen when this object is attacked in the game

  default breakables:
  DEFAULT_PLAYER_BREAKABLE and DEFAULT_ENEMY_BREAKABLE. They are things that can be
  attacked by players and enemies respectively
*/

function Breakable(name){
  this.name = name;
  this.list = [];
  this.detecting = false;
}

/*
  registerObject()
  description: register an object as a breakable groups

  known call:
  - the constructor of Chara
  - the constructor of Enemy
  - the constructor of Gun
  note: ideally each object should join a group a breakable upon its construction
*/
Breakable.prototype.registerObject = function (object){
  this.list.push(object);
  this.list[this.list.length-1].hit = object.hit;
}

/*
  detect(client)
  description:
  - start checking wether the client hits any object of a breakable groups.
  - trigger a certain action if do so
  - the triggered action should be decalred inside the object that got hit

  known call:
  - Bullet.fire()
  - enemy.move()
*/
Breakable.prototype.detect = function (client){
  for (var i=0; i<this.list.length; i++){
    if (client.intersectsMesh(this.list[i],false) && this.list[i].ready){
      console.log("ready?: "+this.list[i].ready);
      console.log(client);
      console.log(this);
      console.log(this.list);

      console.log("HIT!");//obsolte soon
        this.list[i].hit(10);
    }
  }
}

Breakable.prototype.remove = function (object){
  for (var i=0; i<this.list.length; i++){
    if (this.list[i]===object){
      this.list.splice(i,1);
    }
  }
}

//two defaults breakables
var DEFAULT_PLAYER_BREAKABLE = new Breakable("defaultPlayerBreakable");
var DEFAULT_ENEMY_BREAKABLE = new Breakable("defaultEnemyBreakable");
