//this codes captures inputs and redirect them to the proper actions

activeScene.scene.registerBeforeRender(function(){
});

window.onkeydown = function (event){
  var keyCode = event.which||event.keyCode;
  switch (keyCode) {
    case 65://A
      character.left=true;
      break;
    case 68://D
      character.right=true;
      break;
    case 87://W

      character.front=true;
      break;
    case 83://S
      character.back=true;
      break;
    case 32: //Space
      character.jump=true;
      break;
    default:
      break;
    }
}

window.onkeyup = function(event){
  var keyCode = event.which||event.keyCode;
  switch (keyCode) {
    case 65://A
      character.left=false;
      break;
    case 68://D
      character.right=false;
      break;
    case 87://W
      character.front=false;
      break;
    case 83://S
      character.back=false;
      break;
    case 32: //Space
      character.jump=false;
      break;
    default:
      break;
  }
}

var cameraRotationThreshold = window.innerWidth/4;
window.onmousemove = function (){
    if (activeScene.scene.pointerX <=cameraRotationThreshold){
      activeScene.camera.isRotate = "CCW";
    }
    else if (activeScene.scene.pointerX >=window.innerWidth-cameraRotationThreshold){
      activeScene.camera.isRotate = "CW";
    }
    else{
      activeScene.camera.isRotate = false;
    }
  }

window.addEventListener("mousedown", function () {
    character.isShooting=true;
});

window.addEventListener("mouseup", function () {
    character.isShooting=false;
});
