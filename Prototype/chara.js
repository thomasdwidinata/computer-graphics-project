/*description: playable character
  constructor parameter:
  - height: the height (OBSOLETE SOON after real mesh is used)
  - width: the height (OBSOLETE SOON after real mesh is used)
  - health: max health
  - scene: the rendering scene
*/

function Chara(name,height,width, scene, health){

  BABYLON.Mesh.call(this, name, scene);
  this.scene = scene
  this.name=name;
  this.height=height;
  this.width=width;
  this.health = health;
  console.log(this.health);

  this.jumpHeight=15;
  this.isJump=false;
  this.alive = true;

  //------------------------ORIGIN-----------------------------------------------
  this.originHeight = height;
  this.origin= new BABYLON.Mesh.CreateBox("character origin",1, scene);
  this.origin.position.x=-40;
  this.origin.position.z=-40;
  this.origin.moveSpeed = 1;

  //----------------MESH-------------------------------------------------
  this.bodyPart = new Array();
  this.meshCount;
  this.bones = new Array();
  this.ready = false;
  that = this;

  var fenbert = BABYLON.SceneLoader.ImportMesh("","assets/steve/","minecraft-steve.babylon",scene,function(newMeshes){
    for (var i=0; i<newMeshes.length; i++){
      newMeshes[i].scaling = new BABYLON.Vector3(1,1,1);
      that.bodyPart[i]=newMeshes[i];
      that.bones[i]= new BABYLON.Mesh.CreateBox("bone"+i,3, scene);
      that.bones[i].visibility=0;
      that.bodyPart[i].parent= that.bones[i];
      that.bones[i].parent = that.origin;
      that.bodyPart[i].hit = that.hit;
      that.bodyPart[i].ready = true;
      that.bodyPart[i].rotation.y = -Math.PI/2;

      DEFAULT_ENEMY_BREAKABLE.registerObject(that.bodyPart[i]);
    }
    that.meshCount=i;
    that.origin.ready = true;
  });
    this.origin.hit = this.hit;
    this.origin.parent = this;
    this.orientation = this.origin.rotationQuaternion;
    DEFAULT_ENEMY_BREAKABLE.registerObject(this.origin);

  //------------------------SHOOTING BEHAVIOUR-----------------------------------
  //upon the construction of a character, it is ready for shooting
  //shooting happens when the trigger isShooting becomes true
  this.bulletPool = new BulletPool(scene,"DEFAULTPLAYER",10);
  this.isShooting=false;
  this.shootingInterval = 300;
  that = this;
  setInterval(function(){
    // console.log("shoot interval: "+that.isShooting);
    if(that.isShooting){
      var target = scene.pick(scene.pointerX, scene.pointerY);
      var targetVector = new BABYLON.Vector3 (target.pickedPoint.x,0, target.pickedPoint.z);
      var originPosition  = new BABYLON.Vector3 (that.origin.position.x, 0,that.origin.position.z);
      var axis =targetVector.subtract(that.origin.position);
      var angle =Math.atan2(axis.x, axis.z);

      that.origin.rotation.y=angle;
      that.shoot(target.pickedPoint.x, target.pickedPoint.y, target.pickedPoint.z);

    }
  },this.shootingInterval);

  var DEGREE_45_IN_PI=Math.PI/4;
  var DEGREE_135_IN_PI =Math.PI*0.75;
  var DEGREE_90_IN_PI=Math.PI/2;

  var speed = 1;
  this.rightBackVal =  {
    x: -speed * Math.cos(DEGREE_135_IN_PI-this.scene.getCameraByName("sceneCamera").rotation.y),
    z: -speed * Math.sin(DEGREE_135_IN_PI-this.scene.getCameraByName("sceneCamera").rotation.y)
  }

  this.leftBackVal =  {
    x: -speed * Math.cos(DEGREE_45_IN_PI-this.scene.getCameraByName("sceneCamera").rotation.y),
    z: -speed * Math.sin(DEGREE_45_IN_PI-this.scene.getCameraByName("sceneCamera").rotation.y)
  }

  this.leftFrontVal = {
    x : speed * Math.cos(DEGREE_135_IN_PI-this.scene.getCameraByName("sceneCamera").rotation.y),
    z : speed * Math.sin(DEGREE_135_IN_PI-this.scene.getCameraByName("sceneCamera").rotation.y)
  }

  this.rightFrontVal = {
    x: speed * Math.cos(DEGREE_45_IN_PI-this.scene.getCameraByName("sceneCamera").rotation.y),
    z: speed * Math.sin(DEGREE_45_IN_PI-this.scene.getCameraByName("sceneCamera").rotation.y)
  }

  this.rightVal = {
    x: -speed * Math.sin(this.scene.getCameraByName("sceneCamera").rotation.y-DEGREE_90_IN_PI),
    z: -speed * Math.cos(this.scene.getCameraByName("sceneCamera").rotation.y-DEGREE_90_IN_PI)
  }

  this.leftVal = {
    x: -speed * Math.sin(this.scene.getCameraByName("sceneCamera").rotation.y+DEGREE_90_IN_PI),
    z: -speed * Math.cos(this.scene.getCameraByName("sceneCamera").rotation.y+DEGREE_90_IN_PI)
  }

  this.backVal = {
    x: -speed * Math.cos(DEGREE_90_IN_PI-this.scene.getCameraByName("sceneCamera").rotation.y),
    z: -speed * Math.sin(DEGREE_90_IN_PI-this.scene.getCameraByName("sceneCamera").rotation.y)
  }

  this.frontVal = {
    x: speed * Math.cos(DEGREE_90_IN_PI-this.scene.getCameraByName("sceneCamera").rotation.y),
    z: speed * Math.sin(DEGREE_90_IN_PI-this.scene.getCameraByName("sceneCamera").rotation.y)
  }

console.log("last: "+this.health);
}

Chara.prototype = Object.create(BABYLON.Mesh.prototype);
Chara.prototype.constructor = Chara;

/*
move()
description:
1. read movement status (this.left, this.right, this.front, this.back, this.jump)
2.  move the origin accordingly

known call:
- inside keyBinding.js
*/

Chara.prototype.move=function(){
  var xDir=0;
  var zDir=0;
  if (this.right && this.back){
    xDir = this.rightBackVal.x;
    zDir = this.rightBackVal.z
  }else if (this.left && this.back){
      console.log("left-back");
      xDir = this.leftBackVal.x;
      zDir = this.leftBackVal.z;
  }else if (this.right && this.front){
      console.log("right-front");
      xDir = this.rightFrontVal.x;
      zDir = this.rightFrontVal.z;
  }else if (this.left && this.front){
      console.log("left-front");
      xDir = this.leftFrontVal.x;
      zDir = this.leftFrontVal.z;

  }else if (this.right){
    console.log("right");

    xDir = this.rightVal.x;
    zDir = this.rightVal.z;
  }else if(this.left){
    console.log("left");
    xDir = this.leftVal.x;
    zDir = this.leftVal.z;
  }else if(this.back){
    console.log("back");
    xDir = this.backVal.x;
    zDir = this.backVal.z;
  }else if(this.front){
    console.log("front");
    xDir = this.frontVal.x;
    zDir = this.frontVal.z;
  }
  this.origin.position.x += xDir;
  this.origin.position.z +=zDir;
  this.origin.moveDirection = {"xDir": xDir, "zDir": zDir};
  xDir = zDir = 0;

  if(this.jump){
      if(!this.isJump && this.origin.position.y<=this.jumpHeight){
        this.origin.position.y++;
      }
      else{
        this.isJump=true;
      }
  }

  if ((!this.jump||this.isJump) && this.origin.position.y>0){

    this.origin.position.y--;
  //  this.stopJump();
  }
  if(this.origin.position.y<=0){
    this.isJump=false;
  }

}

/*
realMove():
description: copy of move() function to move the meshes
*/
Chara.prototype.realMove=function(x,z){
    for (var i=0; i<this.bones.length; i++){
      this.bodyPart[i].position.x+=x;
      this.bodyPart[i].position.z+=z;
    }
  }

Chara.prototype.realJump = function(){
    for (var i=0; i<this.bones.length; i++){
      this.bodyPart[i].position.y++;
    }
  }

Chara.prototype.stopJump = function(){
  for (var i=0; i<this.bones.length; i++){
    this.bodyPart[i].position.y--;
  }
}

/* shoot()
  description:
  the character shoot a bullet
  1. get the targeted direction by x,y, and z
  2. pop a bullet from its bulletPool (the bullet will move itself)

  know call:
  - inside Character constructor, in the shooting behaviour
*/
Chara.prototype.shoot = function (targetX, targetY, targetZ){
  var that = this;
  this.bulletPool.popBullet(this.origin.position.x, this.origin.position.y, this.origin.position.z, targetX, targetY, targetZ);
}

var that = this;
Chara.prototype.hit = function (damage){
  console.log("in hit: "+that.health);
  if (that.health<=0){
    that.alive = false;
    that.dead();
  }
  console.log("chara hit with damage: "+damage);
  that.health -=damage;
  console.log("chara health: "+that.health);
  HUD.updateHealth(-damage);

}

Chara.prototype.dead = function (){
  this.origin.rotation.z = Math.PI/2;
  alert("Game Over");

}
