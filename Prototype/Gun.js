/* description: a shooting enemy
  constructor parameter:
  - radius = detection radius(a circle)
  - babylonMesh = representational mesh
  - scene = rendering scene
*/

var GUN_MODEL;
function Gun (name, posX, posY, posZ, radius,scene, health){
  BABYLON.Mesh.call(this, name, scene);
  this.scene = scene

  this.position.x=posX;
  this.position.y=posY;
  this.position.z=posZ;
  this.radius=radius;
  this.radSqr=Math.pow(radius,2);

  this.health = health;
  this.alive = true;

  // var that = this;
  // BABYLON.SceneLoader.ImportMesh("coin", "assets/coin/", "Coin.obj", this.scene, function (meshes) {
  //   var m = meshes[0];
  //   m.isVisible = true;
  //   m.scaling = new BABYLON.Vector3(5,5,5);
  //   GUN_MODEL = m;
  //   // var vertexData = GUN_MODEL;//BABYLON.VertexData.CreateCylinder({height:1, diameter:3,tessellation:8,subdivisions:1});
  // });


  var vertexData = BABYLON.VertexData.CreateCylinder({height:1, diameter:3,tessellation:8,subdivisions:1});
  vertexData.applyToMesh(this, false);


  DEFAULT_PLAYER_BREAKABLE.registerObject(this);
  this.ready = true;

  //colorise the representational mesh
  this.setColor = function (Color3){
    this.material=new BABYLON.StandardMaterial("matGun", scene);
    this.material.diffuseColor= Color3;
    this.material.ambientColor = new BABYLON.Color3(1, .6, .8);
  }

  //-----------INSPECTION BEHAVIOR-------------------
  //actions happens when it is being clicked (inspected)
  this.actionManager = new BABYLON.ActionManager(scene);
  this.inspectAct = new BABYLON.ExecuteCodeAction(
    BABYLON.ActionManager.OnPickTrigger,
    function (){
      alert("this is a gun");
    }
  );
  this.actionManager.registerAction(this.inspectAct);

  //-----------SHOOTING BEHAVIOUR-------------------
  //upon construction, there is no target
  //the target can be registered using function registerTarget();
  this.isLocking=false;
  this.target=null;
  this.bulletPool = new BulletPool (scene, "DEFAULTENEMY", 20);
}

Gun.prototype = Object.create(BABYLON.Mesh.prototype);

/*
  setTarget(target)
  description:
  1. receive another mesh as shooting target

  known call:
  - index.html (currently has static target registration. Probably can be improved later)
*/

Gun.prototype.setTarget=function(target){
  this.target = target;
}

/*
  detect()
  description:
  activate the detection of the target
  1. calculte target position
  2. if target is inside radar range, return true; otherwise return false

  known call:
  - collision.js
*/
Gun.prototype.detect=function(){
  if (this.radSqr>=Math.pow((this.position.x-this.target.position.x),2)+Math.pow((this.position.z- this.target.position.z),2)){
    return true;
  }
    return false;
}

/*
  lockTarget()
  description:
  function that should be called after checking target position with Gun.target()
  1. set isLocking to true
  2. start shooting interval
    2.1. (inside shooting interval) pop a bullet from bulletPool with 350 ms interval
    2.2. the bullet will move itself towards the registered target

  know calls:
  - collision.js
*/
Gun.prototype.lockTarget = function () {
  if (!this.isLocking){
    this.isLocking=true;

    var that = this;
    this.shootInterval = setInterval(function(){
      that.bulletPool.popBullet(that.position.x, that.position.y, that.position.z, that.target.position.x, that.target.position.y+3, that.target.position.z);
        },350);
  }
}

/*
  unlockTarget()
  description:
  stop shooting the target by clearing the shootInterval and reseting isLocking status to false
*/
Gun.prototype.unlockTarget = function () {
  if (this.isLocking){
    this.isLocking=false;
    clearInterval(this.shootInterval);
  }
}

Gun.prototype.hit = function(damage){
  this.health-=damage;
  if (this.health<=0){
    this.alive = false;
    this.dead();
  }
}

Gun.prototype.dead = function (){
  DEFAULT_PLAYER_BREAKABLE.remove(this);
  this.dispose();
  alert("you win!");


}
