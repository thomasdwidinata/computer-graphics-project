//this is a moving enemy
//moves from x+ to x- by default

function Enemy(name,height,width, scene)
{
	BABYLON.Mesh.call(this, name, scene);
	this.scene = scene;
	this.name=name;
	this.height=height;
	this.width=width;
	this.health = 30;
	this.isMoving = true;

	var vertexData = BABYLON.VertexData.CreateSphere({diameter: 5});
	vertexData.applyToMesh(this, false);


	this.position.y = 0;
	this.position.x =activeScene.width/2;
	this.breakable = DEFAULT_ENEMY_BREAKABLE;
	this.move = function(){


		if (this.isMoving){
			this.breakable.detect(this);
			this.position.x--;
			if(this.position.x<-activeScene.width/2){
				console.log(this.name+": "+this.position.z);
				this.position.x=activeScene.width;
			}
		}
	}
	DEFAULT_PLAYER_BREAKABLE.registerObject(this);
	this.ready =true;

}

Enemy.prototype = Object.create(BABYLON.Mesh.prototype);
Enemy.prototype.constructor = Enemy;


// var that = this;
Enemy.prototype.hit = function (damage){
	console.log(this);

	this.health -=damage;
	if (this.health<=0){
		this.dead();
	}
	console.log(this.health);
}

Enemy.prototype.dead = function (){
	this.isMoving = false;
	this.mat=new BABYLON.StandardMaterial("matEnemy", this.scene);
	this.mat.diffuseColor= new BABYLON.Color3(1,0,0);
	//this.material.ambientColor = new BABYLON.Color3(1, .6, .8);
	this.material = this.mat;
	var that = this;
	setTimeout(function(){
		that.mat.diffuseColor = new BABYLON.Color3(1,0.8,0);
		that.scaling = new BABYLON.Vector3(2,2,2);
		that.material = that.mat;
	}, 500);

	setTimeout(function(){
		that.dispose();
	}, 1000);
}
